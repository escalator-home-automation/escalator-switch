#include "MillisTimer.h" // https://github.com/bhagman/MillisTimer/issues
#include "EscalatorSwitch.h"

uint8_t ac_data_point_idx=0;
#define TOTAL_DATA_POINTS_MINUS_1 40 // did not pass the solder iron filter requirement
uint8_t ac_data_point[TOTAL_DATA_POINTS_MINUS_1+1];

void check_daily_services(MillisTimer& timer)
{
  ac_data_point_idx++;
  if(ac_data_point_idx>TOTAL_DATA_POINTS_MINUS_1)
    ac_data_point_idx=0;

  ac_data_point[ac_data_point_idx]=0;
  if(digitalRead(coil_pin))
    ac_data_point[ac_data_point_idx]=1;

  uint8_t ac_total=0;
  for(uint8_t i=0;i<=TOTAL_DATA_POINTS_MINUS_1;i++)
    ac_total+=ac_data_point[i];

  if (ac_total>2)
    turn_on();
  else
    turn_off();
}

void turn_on()
{
    digitalWrite(relay_coil1_pin,  HIGH);
    digitalWrite(relay_coil1_pinn, HIGH);
    delay(50);
    digitalWrite(relay_coil1_pin,  LOW);
    digitalWrite(relay_coil1_pinn, LOW);
}

void turn_off()
{
    digitalWrite(relay_coil2_pin,  HIGH);
    digitalWrite(relay_coil2_pinn, HIGH);
    delay(50);
    digitalWrite(relay_coil2_pin,  LOW);
    digitalWrite(relay_coil2_pinn, LOW);
}

MillisTimer timer_rtc = MillisTimer(200);

void setup() {
  esSetPinMode();
  
 // its useful to have the relay toggle at least once at start-up
  toggle_relay();
  delay(1000);
  toggle_relay();

  timer_rtc.expiredHandler(check_daily_services);
  timer_rtc.start();
}


void loop() {
  timer_rtc.run();
  heart_beat();

  digitalWrite(ac_indicator_pin, digitalRead(coil_pin));
}
