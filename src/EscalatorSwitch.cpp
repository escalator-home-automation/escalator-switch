#include "EscalatorSwitch.h"

boolean tr_state = false;
void toggle_relay()
{
  if (tr_state == false)
  {
    tr_state = true;

    // trigger coil direction 1
    digitalWrite(relay_coil1_pin,  HIGH);
    digitalWrite(relay_coil1_pinn, HIGH);
    delay(50);
    digitalWrite(relay_coil1_pin,  LOW);
    digitalWrite(relay_coil1_pinn, LOW);
  }
  else
  {
    tr_state = false;

    // trigger coil direction 2
    digitalWrite(relay_coil2_pin,  HIGH);
    digitalWrite(relay_coil2_pinn, HIGH);
    delay(50);
    digitalWrite(relay_coil2_pin,  LOW);
    digitalWrite(relay_coil2_pinn, LOW);
  }
}

uint8_t hb=0;
uint8_t hb2=0;
enum HEART_BEAT_DIRECTION { increasing, decreasing } heart_beat_direction = increasing;

void heart_beat()
{
  hb2++;
  if(hb2!=0)
    return;

  if(heart_beat_direction == increasing)
    hb++;
  else
    hb--;

  if(hb>20)
    heart_beat_direction = decreasing;
    
  if(hb<5)
    heart_beat_direction = increasing;

  analogWrite(heart_beat_pin, hb);
}

void esSetPinMode()
{
    pinMode(coil_pin, INPUT);

  pinMode(ac_indicator_pin,  OUTPUT);

  pinMode(relay_coil1_pin,  OUTPUT);
  pinMode(relay_coil1_pinn, OUTPUT);
  pinMode(relay_coil2_pin,  OUTPUT);
  pinMode(relay_coil2_pinn, OUTPUT);
  
  pinMode(heart_beat_pin, OUTPUT);
  pinMode(rtc_vcc_pin, OUTPUT);

  // put the relay in a determinate state
  digitalWrite(relay_coil1_pin,  LOW);
  digitalWrite(relay_coil1_pinn, LOW);
  digitalWrite(relay_coil2_pin,  LOW);
  digitalWrite(relay_coil2_pinn, LOW);

  analogWrite(heart_beat_pin, 12);
}
