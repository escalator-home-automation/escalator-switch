# Escalator Switch

## Overview

Is there anything in your house that you mindlessly turn on at certain times and off at others?  Does the idea of a device that reaches out to the internet to control such a simple function seem absurd?  Does applying additional complexity like an app on your phone seem akin to killing a fly with a shot-gun?  The Escalator Switch is automation that is completely transparent to the user.  There is no app, no internet connection, no new buttons and no explanation for the house-sitter.  Unless you happen to notice it changing the state of the light, you would never know it's there at all.

The Escalator Switch combines a power supply, non-invasive binary current sensor, over-center relay, microcontroller and real-time clock in a package that is screwed to the back of a 3-way light switch.  The relay and the 3-way switch are wired such that either the micro-controller or a person can toggle the state of the load.  The current sensor indicates to the microcontroller when the AC load is on.  In this way, automation can co-exist with the standard functionality of the light switch.  When automation calls for light, the microcontroller throws the relay only when it determines that the light is not already on.  When a person wants to change the state of the light, they flip the switch.  Like an escalator, the switch provides automation in an intuitive package and should the automation fail, you still have the basic pre-automation capability, a manual light switch.

<img height=300 src="docs/images/prototype-2022-01-issue-31.png"/><img height=300 src="docs/images/2-deployed-switches.png"/>

The picture above on the left shows a single Escalator Switch. The picture on the right shows two deployed Escalator Switches in a 3-gang box next to a dimmer switch. The middle switch turns on my front porch light at an hour before sunset and turns it off at 8pm. The right switch turns on a pair of security lights at sunset and turns them off at sunrise. These switches have been deployed since February of 2021.

This product is a platform for simple DIY home automation providing a bridge between low-power, dc, control oriented folks and line-level power. While it includes the elements required for many simple automation tasks, it also includes  two connectors that provide i2c access to 1-input (AC load presence) and 1-output (over-center relay). It can be deployed with a light switch in a rough box or without a light switch in other applications. An Arduino library and example sketches are open source and available from the Escalator Home Automation project.

**What stage of development is this project in**
Since October of 2020 I've produced [10 prototype versions](https://gitlab.com/eslatt/escalator-home-automation/-/blob/master/docs/prototype-history.md), each with printed circuit boards from [pcbway.com](http://pcbway.com/). The updates have been related to ease of fabrication, form factor and stability. I've deployed five of these devices in my house since February of 2021. Arduino and Python code are in place and protected by [CICD automation](https://gitlab.com/eslatt/escalator-home-automation/-/pipelines) that runs in GitLab. Much of the work I have [queued](https://gitlab.com/eslatt/escalator-home-automation/-/boards) is related to organizing the [Escalator Home Automation project](https://gitlab.com/eslatt/escalator-home-automation), deploying the Arduino code into the Arduino ecosystem and writing additional example sketches. Also, I may make a UL Mark evaluation effort depending on feedback from SparkFun.

<img height=150 src="docs/images/prototypes-1-5.png"/>

<img height=150 src="docs/images/prototypes-6-10.png"/>

## Contributors

* Please [submit a GitLab Issue to this project](https://gitlab.com/eslatt/escalator-home-automation/-/issues/new) if you would like to contribute.

---

<img width=60 src="https://www.oshwa.org/wp-content/uploads/2014/03/oshw-logo-outline.svg"/><a href="http://creativecommons.org/licenses/by-sa/4.0/deed.en_US"><img src="https://i0.wp.com/i.creativecommons.org/l/by-sa/4.0/88x31.png?w=790&ssl=1"/></a>

